This repository is for Front-end assignment for DamoGO!<br>
Do not commit or PR here.

- Firstly clone this repo to set up your localhost for RN fetches
  - `npm install`
  - Choose how you want to get data in your app. You can use REST API or GraphQL API. If you choose to use GraphQL API, please use graphql client such as apollo or urql.
  - `npm run server` for GraphQL server (Preferred)
  - `npm run json-server` for REST API

Graphql server will open graphiql playground in `http://localhost:4000/`.
> ### You can query stores data using `allStores` query.
<br>

REST API will be served in `http://localhost:5000`.
> ### You should fetch stores from `http://localhost:5000/stores` endpoint.
<br>

- Build RN app accourding to the https://www.figma.com/file/8452bJBrGDI0zbZyJgFHJN/DamoGO-Interview?node-id=0%3A1 with the data from the server you just have set up above.
- > `Play` the design in Figma if you are not sure what is happening
- TypeScript is a big plus.
- Pictures' placeholders are in Figma.
- Tab navigator, leave other screens empty
- Home icon and address are dummy placeholders
- Search
- Categories, generate
- Store Branches, generate from the data on the fly
- Store lists
  - Just joined, latest id latest joined store
  - Favorites, sort by amount of offers >
  - Don't miss out, sort by amount of offers < (Do not include 0 offers)
- dummy placeholdered Cart
- Upon finishing upload RN app to your personal git and send repo URL to ilya@damogo.co.kr

For TypeScript
```
interface Store {
      id: number,
      store_name: string,
      store_image: string,
      store_logo: string,
      store_business: string,
      isFav: boolean,
      delivery_price: number,
      num_offers: number,
      num_reviews: number,
      store_type: string
    }
```
